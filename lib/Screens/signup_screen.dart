import 'dart:io';
import 'package:attendence_tracker/Screens/login_screen.dart';
import 'package:attendence_tracker/Database/database.dart';
import 'package:attendence_tracker/Screens/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../Models/user_info.dart';
import '../Utils/validator.dart';


class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final TextEditingController _name = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _mobileNo = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();
  final TextEditingController _dob = TextEditingController();
  String imgPath = '';
  bool passToggle = true;
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size / 100;

    return Scaffold(
        appBar: AppBar(
          title: const Text("Sign In Page"),
          centerTitle: true,
        ),
        body: Container(decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/login.png'),fit: BoxFit.cover
        )),

      child: SingleChildScrollView(
        child: Form(
          key: _formkey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: size.width * 100,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: CircleAvatar(
                        backgroundColor: Colors.blue,
                        radius: size.height * 8,
                        backgroundImage: imgPath.isNotEmpty
                            ? FileImage(File(imgPath))
                            : null,
                        child: imgPath.isEmpty
                            ? Icon(
                                Icons.person,
                                size: size.height * 10,
                                color: Colors.white,
                              )
                            : null,
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    // Positioned(
                    //     top: size.height * 10,
                    //     right: size.width * 32.5,
                    //     child: InkWell(
                    //       onTap: _pickImage,
                    //       child: CircleAvatar(
                    //         backgroundColor: Colors.black.withOpacity(.5),
                    //         radius: size.height * 2,
                    //         child: Icon(
                    //           Icons.edit,
                    //           color: Colors.white,
                    //           size: size.height * 2.5,
                    //         ),
                    //       ),
                    //     ))
                  ],
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                child: TextFormField(
                  controller: _name,
                  keyboardType: TextInputType.text,
                  decoration: const InputDecoration(
                      border: const OutlineInputBorder(),
                      prefixIcon: Icon(Icons.person), labelText: "Name"),
                  validator: (value) {
                    bool nameValid = nameValidator.hasMatch(value!);
                    if (value.isEmpty) {
                      return "Enter name";
                    }  else if (!nameValid) {
                      return "Enter valid name";
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                child: TextFormField(
                  controller: _email,
                  keyboardType: TextInputType.emailAddress,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                      prefixIcon: Icon(Icons.email), labelText: "Email"),
                  validator: (value) {
                    bool emailValid = emailValidator.hasMatch(value!);
                    if (value.isEmpty) {
                      return "Enter Email";
                    } else if (!emailValid) {
                      return "Enter valid email";
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                child: TextFormField(
                  maxLength: 10,
                  controller: _mobileNo,
                  keyboardType: TextInputType.phone,
                  decoration: const InputDecoration(
                    counterText: '',
                    border: const OutlineInputBorder(),
                      prefixIcon: Icon(Icons.phone), labelText: "Mobile No."),
                  validator: (value) {
                    bool isMobileValid = phoneValidator.hasMatch(value!);
                    if (value!.isEmpty) {
                      return "Enter mobile no.";
                    } else if (value.isNotEmpty && value.length != 10) {
                      return "Mobile no. should be exactly of 10 digits.";
                    } else if (!isMobileValid) {
                      return "Enter valid mobile number";
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                child: TextFormField(
                  controller: _password,
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: passToggle,
                  obscuringCharacter: '*',
                  decoration: InputDecoration( labelText: "Password",
                    border: const OutlineInputBorder(),
                     prefixIcon: const Icon(Icons.lock),
                      suffixIcon: InkWell( onTap: (){
                        setState(() {
                          passToggle = !passToggle;
                        });
                      },
                        child: Icon( passToggle ? Icons.visibility : Icons.visibility_off),
                  ),
                     ),
                  validator: (value) {
                    bool passwordValid = passwordValidator.hasMatch(value!);
                    if (value.isEmpty) {
                      return "Enter password";
                    } else if (_password.text.length < 8) {
                      return "Password should be more than 8 letter";
                    } else if(!passwordValid)
                      {
                        return "Your password should contain:\n * 1 special character * lower case * upper case * 1 digit";
                      }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15, left: 10, right: 10),
                child: TextFormField(
                  obscureText: true,
                  obscuringCharacter: '*',
                  controller: _confirmPassword,
                  keyboardType: TextInputType.text,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                      prefixIcon: Icon(Icons.lock), labelText: "Confirm Password"),

                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please re-enter the password";
                    } else if (_password.text != _confirmPassword.text) {
                      return "password do not match";
                    }
                    return null;
                  },
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: _registerUser,
                child: Container(
                  height: size.height * 6,
                  width: size.width * 60,
                  decoration: BoxDecoration(
                    color: Colors.indigo,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: const Center(
                    child: Text(
                      "Sign Up",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Already have an account?",
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  TextButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const LoginScreen(),
                            ));
                      },
                      child: const Text(
                        "Log In",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    ));
  }

  _registerUser() async {
    bool isValidInfo = _formkey.currentState?.validate() ?? false;
    if (!isValidInfo) {
      return;
    }

    List<UserInfo> userList = await MyDatabase.getUserInfo(_email.text);
    if (userList.isNotEmpty) {
      if (mounted) {
        showMessage(context, "This email already exists !");
      }
      return;
    }

    try {

      currentUserInfo = UserInfo(
          _name.text, _email.text, _mobileNo.text, _password.text, imgPath,_dob.text);
      await MyDatabase.addUserInfo(currentUserInfo);

      if (mounted) {
        _name.clear();
        _email.clear();
        _mobileNo.clear();
        _password.clear();
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => const LoginScreen(),
            ),
            (route) => false);
        showMessage(context, "Information added successfully");
      }
    } catch (e) {
      if (mounted) {
        showMessage(context, "Something went wrong");
      }
      return;
    }
  }
}
